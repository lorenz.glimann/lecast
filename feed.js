const fs = require("fs");
const RSS = require("rss-generator");

// {
//     title: `${req.query.id}`,
//         feed_url: `${req.protocol}://${req.hostname}:${PORT}/${req.query.id}/rss.xml`,
//     site_url: `${req.protocol}://${req.hostname}:${PORT}/${req.query.id}/`
// }

function createFeed(id, metadata) {
    let feeddata = {
        meta: metadata,
        items: []
    };
    writeFeed(id, feeddata);
}

function addtoFeed(id, item) {
    let feeddata = JSON.parse(fs.readFileSync(`${__dirname}/public/${id}/feed.json`).toString());
    // add item to feed
    feeddata.items.push(item);
    // update feed files
    writeFeed(id, feeddata);
}

function feed2xml(feeddata) {
    const feed = new RSS(feeddata.meta);
    for (let i = 0; i < feeddata.items.length; i++) {
        feed.item(feeddata.items[i]);
    }
    return feed.xml();
}

function writeFeed(id, feeddata) {
    // update rss and json
    fs.writeFileSync(`${__dirname}/public/${id}/rss.xml`, feed2xml(feeddata));
    fs.writeFileSync(`${__dirname}/public/${id}/feed.json`, JSON.stringify(feeddata));
}

module.exports = {
    createFeed,
    addtoFeed
};