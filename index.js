const express = require("express");
const ffmpeg = require("fluent-ffmpeg");
const bodyParser = require("body-parser");
const fs = require("fs");
const fileUpload = require("express-fileupload");
const app = express();
const feed = require("./feed");

const PORT = 8080;
const home = `${__dirname}/public`;

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: false}));

// parse application/json
app.use(bodyParser.json());

//support parsing of application/x-www-form-urlencoded post data
app.use(
    fileUpload({
        useTempFiles: true,
        tempFileDir: "/tmp/",
    })
);

app.set("view engine", "pug");

ffmpeg.setFfmpegPath("/usr/bin/ffmpeg");
ffmpeg.setFfprobePath("/usr/bin/ffprobe");
ffmpeg.setFlvtoolPath("/usr/bin/ffplay");

function convertToMP3(id, file, cb) {
    // replace file extension with mp3
    let fileName = `${file.name.split(".").slice(0, -1).join(".")}.mp3`;
    // convert to mp3
    ffmpeg("tmp/" + file.name)
        .noVideo()
        .audioCodec("libmp3lame")
        .saveToFile(`${home}/${id}/${fileName}`)
        .on("end", function (stdout, stderr) {
            console.log("Finished");
            cb(fileName);
        })
        .on("error", function (err) {
            console.log("an error happened: " + err.message);
            // remove video file
            fs.unlinkSync("tmp/" + file.name);
        });
}

app.use("/res", express.static(__dirname + "/res"));

// show known podcast lists
app.get("/", (req, res) => {
    console.log("home");
    res.render("index", {});
});
app.get("/about", (req, res) => {
    console.log("about");
    res.render("about", {});
});
// view feed
app.get("/:id", (req, res) => {
    console.log("feed home");
    const id = req.params.id;
    const feedDirectory = `${__dirname}/public/${id}`;
    if (!fs.existsSync(feedDirectory)) {
        fs.mkdirSync(feedDirectory);
        feed.createFeed(id, {
            title: `${id}`,
            feed_url: `${req.protocol}://${req.hostname}:${PORT}/${id}/rss.xml`,
            site_url: `${req.protocol}://${req.hostname}:${PORT}/${id}/`
        });
    }
    res.render("feed", {
        id: id,
        rsspath: `${req.protocol}://${req.hostname}:${PORT}/${id}/rss.xml`
    });
});
// receive file and convert
app.post("/:id", (req, res) => {
    console.log("convert");
    const id = req.params.id;
    let file = req.files.file;

    // save video file to tmp directory
    file.mv("tmp/" + file.name, function (err) {
        if (err) return res.sendStatus(500);
        console.log("File Uploaded successfully");
    });

    convertToMP3(id, file, (fileName) => {
        res.send(200);
        // add converted file to feed
        feed.addtoFeed(id, {
            title: file.name,
            enclosure: {url: `${req.protocol}://${req.hostname}:${PORT}/${id}/${fileName}`}
        });
        // remove video file
        fs.unlinkSync(`tmp/${file.name}`);
    });
});
// serve directory with podcasts statically
app.use("/", express.static(__dirname + "/public")); // all files except root should be served statically

app.listen(PORT);
